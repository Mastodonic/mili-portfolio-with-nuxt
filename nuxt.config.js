var SvgStore = require('webpack-svgstore-plugin');

module.exports = {
    head: {
        title: 'starter',
        meta: [
            {
                charset: 'utf-8'
            }, {
                name: 'viewport',
                content: 'width=device-width, initial-scale=1'
            }, {
                hid: 'description',
                content: 'Nuxt.js project'
            }
        ],
        link: [
            {
                rel: 'icon',
                type: 'image/x-icon',
                href: 'favicon.ico'
            }
        ]
    },
    build: {
        plugins: [// create svgStore instance object
            new SvgStore({
                // svgo options
                svgoOptions: {
                    plugins: [
                        {
                            removeTitle: true
                        }
                    ]
                },
                prefix: 'shape-'
            })]
    },
    css: [
        {
            src: '~assets/sass/style.scss',
            lang: 'sass'
        }
    ],
    loading: false
};
