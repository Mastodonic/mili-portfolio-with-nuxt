const resolve = require('path').resolve;

module.exports = function(routes) {
    routes.push({
        name: 'custom',
        path: '*',
        component: resolve(__dirname, 'pages/404.vue')
    })
}
