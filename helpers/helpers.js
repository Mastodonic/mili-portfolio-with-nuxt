export default {
    scrollTop() {
        return (window.pageYOffset !== undefined) ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop;
    },
    windowHeight(){
        return window.innerHeight || document.documentElement.clientHeight;
    },
    documentHeight() {
        var B = document.body,
            H = document.documentElement,
            height;
        if (typeof document.height !== 'undefined') {
            height = document.height;
        } else {
            height = Math.max( B.scrollHeight, B.offsetHeight,H.clientHeight, H.scrollHeight, H.offsetHeight );
        }
        return height;
    },
    isBrowser() {
        return process.BROWSER_BUILD ? true : false;
    },
    elementPosition (el) {
        var rect = el.getBoundingClientRect();
        return {
            top: rect.top,
            left: rect.left,
            bottom: rect.bottom,
            right: rect.right,
        };
    }
};
