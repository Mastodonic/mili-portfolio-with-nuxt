export default {
    initSprite() {
        if (process.BROWSER_BUILD) {
          var __svg__ = {
              path: './assets/svg/**/*.svg',
              name: 'assets/svg/[hash].logos.svg'
          };
          require('webpack-svgstore-plugin/src/helpers/svgxhr')(__svg__);
        }
    }
}
