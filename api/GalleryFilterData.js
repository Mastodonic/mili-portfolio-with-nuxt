export default {
    filters: [
        {
            path: 'selection',
            name: 'Selection'
        },
        {
            path: 'morocco',
            name: 'Morocco'
        },
        {
            path: 'spain',
            name: 'Spain'
        },
        {
            path: 'england',
            name: 'England'
        },
        {
            path: 'scotland',
            name: 'Scotland'
        }
    ]
};
