import ImagesData from './ImagesData';
import FilterData from './GalleryFilterData';

export default {
    getImages(category) {
        if (category) {
            return ImagesData.filter((el) => {
                return el.categories.indexOf(category) > -1;
            });
        }
    },
    getImageById(id) {
        return ImagesData.find((el) => {
            return el.id == id;
        });
    },
    getFilterList() {
        return FilterData.filters;
    }
};
