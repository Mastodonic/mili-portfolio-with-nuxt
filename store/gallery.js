import Vue from 'vue';
import Api from '../api/api';
import Helpers from '../helpers/helpers';
import throttle from 'lodash.throttle';

const state = {
    currentCategory: null,
    filterList: [],
    imageLoadOffset: 200, //px
    imagesList: [],
    imagesPosition: [],
    rowHeight: 280,
    scrollThrottle: 500 //ms
};

const mutations = {
    loadGrid(state, payload) {
        state.imagesList = [];
        state.imagesList = payload;
    },
    setImagePosition(state, payload) {
        if (state.imagesPosition !== payload) {
            state.imagesPosition = payload;
        }
    },
    setCurrentCategory(state, payload){
        state.currentCategory = payload;
    },
    setFilters(state, payload) {
        state.filterList = payload;
    },
    setImagesToLoad(state) {
        // Compare the imagepositions array with the current
        // scrollTop and add set toBeLoaded property to true on visible images
        let whAndoffset = Helpers.windowHeight() + Helpers.scrollTop() + state.imageLoadOffset;

        let positions = state.imagesPosition;
        for (let i = 0; i < positions.length; i += 1) {
            if (positions[i] <= whAndoffset) {
                // We have to set the value using Vue instead of doing
                // directly to enable Reactivity
                Vue.set(state.imagesList[i], 'toBeLoaded', true);
            }
        };
    }
};

const getters = {
    imagesListComplete(state) {
        let array = state.imagesList.map((item) => {
            let img = item;
            img.aspectRatio = (img.width / img.height);
            img.aspectRatioPercentage = (img.height / img.width * 100).toFixed(2);
            img.growRatio = (img.aspectRatio * 100).toFixed(2);
            img.orientation = img.aspectRatio <= 1 ? 'portrait' : 'landscape';
            return img;
        });
        return array;
    }
};

const actions = {
    loadGrid(context, category) {
        let imagesArray = Api.getImages(category.toLowerCase());
        context.commit('loadGrid', imagesArray);
    },
    getImagesPosition(context, images) {
        let imagePositions = [];

        for (let i = 0; i < images.length; i += 1) {
            var offset = Helpers.elementPosition(images[i]);
            imagePositions.push(offset.bottom);
        }
        context.commit('setImagePosition', imagePositions);
    },
    getFilters(context) {
        let filters = Api.getFilterList();
        context.commit('setFilters', filters);
    },
    loadImageOnScroll(context) {
        window.addEventListener('scroll', throttle(() => {
            context.commit('setImagesToLoad');
        }, context.state.scrollThrottle));
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};
